import os


class SessionData():

    def __init__(self, path, session_num = "*", subject_name = "*"):
        self.subject_name = subject_name
        self.session_num = session_num
        self.path = path


    def perfile_info(self, file_name):
        conversation = []
        with open(file_name) as file_reader:
            for line in file_reader:
                if line.startswith("P:"):
                    line = line.strip("P:")
                    line = line.split('[')[0].rstrip()
                    conversation.append(line)

        return {file_name: conversation}



    def __iter__(self):
        if self.session_num == "*" and self.subject_name == "*":
            for dirpath, dirname, filenames in os.walk(self.path):
                for filename in filenames:
                    if os.path.isfile(os.path.join(dirpath, filename)):
                        yield self.perfile_info(os.path.join(dirpath, filename))


        if self.session_num == "*" and self.subject_name !="*":
            for dirpath, dirname, filenames in os.walk(self.path):
                for filename in filenames:
                    if self.subject_name.lower() in filename.lower():
                        yield self.perfile_info(os.path.join(dirpath, filename))


        if self.subject_name == "*" and self.session_num!="*":
            for dirpath, dirname, filenames in os.walk(self.path):
                session_path = os.path.join(dirpath, self.session_num)
                for file_name in os.listdir(session_path):
                    if os.path.isfile(os.path.join(session_path, file_name)):
                        yield self.perfile_info(os.path.join(session_path, file_name))
                break


if __name__ ==  "__main__":
    for i in SessionData(path="./inputs", session_num="session1", subject_name="*"):
        print(i)
        print("###########")


    # sss = SessionData(path="./inputs")
    # sss.perfile_info("/home/rohola/Codes/Ryan/RyanCBT/analysis/inputs/session1/don.txt")

