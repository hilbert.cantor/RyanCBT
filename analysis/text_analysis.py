from analysis.sessiondata import SessionData
import spacy
from analysis.visualization import plotlyvisualize as plt
from analysis.corenlp.client import Client
from gensim import corpora
from spacy.lang.en.stop_words import STOP_WORDS
from gensim import models
import numpy as np
from sklearn import datasets, linear_model
from sklearn.metrics import mean_squared_error, r2_score


class TextAnalysis():

    def __init__(self, path):
        self._path = path
        self._nlp = spacy.load('en')

        # words = self._get_words()
        # self.dictionary = corpora.Dictionary([words])
        # stop_ids = [self.dictionary.token2id[stopword] for stopword in
        #             STOP_WORDS if stopword in self.dictionary.token2id]
        # #once_ids = [tokenid for tokenid, docfreq in self.dictionary.dfs.items() if docfreq == 1]
        # self.dictionary.filter_tokens(stop_ids)
        # self.dictionary.compactify()


    def get_documents(self, participant):
        vectors = []
        for document in self._documents(participant):
            en_abstract = self._nlp(document)
            words = [token.lemma_ for token in en_abstract]
            vectors.append(self.dictionary.doc2bow(words))

        return vectors


    def read_corpus(self, participant):
        corpus = []
        for vector in self.get_documents(participant):
            corpus.append(vector)

        self.dictionary.save('ddd.dict')
        dictionary = corpora.Dictionary.load('ddd.dict')
        return corpus, dictionary

    def _get_words(self):
        words = []
        sessoin_data = SessionData(self._path, session_num="*", subject_name="*")
        for session_conversations in sessoin_data:
            for filename in session_conversations:
                print(filename)
                conversation = session_conversations[filename]
                for sentence in conversation:
                    tokens = [token.text for token in self._nlp(sentence) if token.text.rstrip() != ""]
                    words += tokens

        return words#list(set(words))


    def _documents(self, participant):
        documents = []
        for session_conversations in SessionData(self._path, "*", subject_name=participant):
            for filename in session_conversations:
                conversation = session_conversations[filename]
                documents.append(" ".join(conversation))

        return documents

    def lda_analysis(self, participant):
        corpus, dictionary = self.read_corpus(participant)

        # alpha = np.ones(num_topics) * alpha_value
        # # eta = np.ones(num_topics)*eta_value
        # print(alpha)
        num_topics = 20
        num_topics_to_show = 10
        num_words = 10

        lda = models.ldamodel.LdaModel(corpus=corpus, id2word=dictionary, num_topics=num_topics,
                                       update_every=1, passes=6, alpha=0.1, eta='auto')
        topics = []
        for topic in lda.print_topics(num_topics=num_topics_to_show, num_words=num_words):
            topics.append([(item.split('*')[1], float(item.split('*')[0])) for item in topic[1].split('+')])

        for topic in topics:
            for word, p in topic:
                print(word)
            print('\n')


    def wordcount_per_person(self, subject_name):
        num_words = {}
        sessoin_data = SessionData(self._path, "*", subject_name=subject_name)
        for session_conversations in sessoin_data:
            num_word = 0
            for filename in session_conversations:
                conversation = session_conversations[filename]
                for sentence in conversation:
                    tokens = [token.text for token in self._nlp(sentence) if token.text.rstrip() != ""]
                    num_word += len(tokens)

                session_num = filename.split("/")[-2]
                num_words[session_num] = num_word

        return num_words

    def avg_wordcount_per_person(self, subject_name):
        num_words = []
        sessoin_data = SessionData(self._path, "*", subject_name=subject_name)
        for session_conversations in sessoin_data:
            avg_num_word = 0
            for filename in session_conversations:
                conversations = session_conversations[filename]
                for sentence in conversations:
                    tokens = [token.text for token in self._nlp(sentence) if token.text.rstrip() != ""]
                    avg_num_word += len(tokens)

                avg_num_word = avg_num_word / len(conversations)
                session_num = int(filename.split("/")[-2].strip("session"))
                num_words.append((session_num, avg_num_word))

        return num_words

    def wordcount_per_session(self, session_num):
        num_words = {}
        sessoin_data = SessionData(self._path, session_num, subject_name="*")
        for session_conversations in sessoin_data:
            num_word = 0
            for filename in session_conversations:
                conversation = session_conversations[filename]
                for sentence in conversation:
                    tokens = [token.text for token in self._nlp(sentence) if token.text.rstrip() != ""]
                    num_word += len(tokens)

                session_num = filename.split("/")[-1].split('.')[0]
                num_words[session_num] = num_word

        return num_words

    def sentiment_per_session(self, subject_name):
        config = {}
        config['ip'] = "http://localhost"
        config['port'] = "9000"
        config['jar_dir'] = "/home/rohola/Codes/Ryan/RyanCBT/analysis/libs/stanford-corenlp-full-2018-02-27/*"
        client = Client(config)

        all_sentiments = []

        sessoin_data = SessionData(self._path, "*", subject_name=subject_name)

        for session_conversations in sessoin_data:
            avg_sentiment = 0
            num_positive_sentences = 0
            num_negative_sentences = 0
            num_neutral_sentences = 0
            num_sentences = 0
            for filename in session_conversations:
                conversations = session_conversations[filename]

                for conversation in conversations:
                    texts = self._nlp(conversation)

                    for sentence in texts.sents:
                        #
                        if sentence.text.rstrip() != "":
                            print(sentence.text)
                            sentiment = client.get_sentence_sentiment(sentence.text)
                            num_sentences +=1
                            avg_sentiment += len(sentiment[1])
                            if sentiment[0].lower() == "positive":
                                num_positive_sentences += 1
                            elif sentiment[0].lower() == "negative":
                                num_negative_sentences += 1
                            elif sentiment[0].lower() == "neutral":
                                num_neutral_sentences += 1
                            elif sentiment[0].lower() == "verypositive":
                                print("verypositive")
                                num_positive_sentences += 1
                            elif sentiment[0].lower() == "verynegative":
                                print("verynegative")
                                num_negative_sentences -= 1


                session_num = int(filename.split("/")[-2].strip("session"))
                print(session_num, len(conversations))

                all_sentiments.append((session_num, {"positive": num_positive_sentences/num_sentences,
                                                     "negative": num_negative_sentences/num_sentences,
                                                     "neutral": num_neutral_sentences/num_sentences}))

        return all_sentiments



    def linear_regression(self, X, Y):
        X = np.array(X).reshape((-1,1))
        Y = np.array(Y)
        print(X)
        print(Y)
        regr = linear_model.LinearRegression()
        regr.fit(X, Y)
        Y_regressed = regr.predict(X)

        # Y_regressed = regr.coef_*X + regr.intercept_
        print("coef", regr.coef_)
        print("intercept", regr.intercept_)

        print("Mean squared error:", mean_squared_error(Y, Y_regressed))

        print('Variance score: ', r2_score(Y, Y_regressed))

        return regr.coef_, Y_regressed




if __name__ == "__main__":
    result_dir = "/home/rohola/Codes/Ryan/RyanCBT/analysis/results/"
    text_analysis = TextAnalysis(path="/home/rohola/Codes/Ryan/RyanCBT/analysis/inputs/")
    # inputs = {}
    # for participant in ["katie", "don", "christine", "linda"]:
    #     participant_result = text_analysis.avg_wordcount_per_person(subject_name=participant)
    #     inputs[participant] = sorted(participant_result, key=lambda tup: tup[0])
    #
    # plt.grouped_bar_chart_plot_based_on_person(inputs, ["katie", "don", "christine", "linda"], result_dir)

    for participant in  ["linda"]:
        all_sentiments  = text_analysis.sentiment_per_session(participant)
        result = sorted(all_sentiments, key=lambda tup: tup[0])
        print(result)
        #result = [(1, {'negative': 0.2631578947368421, 'neutral': 0.631578947368421, 'positive': 0.10526315789473684}), (2, {'negative': 0.2773109243697479, 'neutral': 0.5294117647058824, 'positive': 0.19327731092436976}), (3, {'negative': 0.2903225806451613, 'neutral': 0.4838709677419355, 'positive': 0.22580645161290322}), (4, {'negative': 0.32786885245901637, 'neutral': 0.47540983606557374, 'positive': 0.19672131147540983}), (5, {'negative': 0.35294117647058826, 'neutral': 0.49019607843137253, 'positive': 0.1568627450980392}), (6, {'negative': 0.4, 'neutral': 0.45714285714285713, 'positive': 0.14285714285714285}), (7, {'negative': 0.20833333333333334, 'neutral': 0.5833333333333334, 'positive': 0.20833333333333334})]


        x = [item[0] for item in result]
        y_positive = [item[1]["positive"] for item in result]
        y_negative = [item[1]["negative"] for item in result]

        pos_coef, y_positive_regressed = text_analysis.linear_regression(x, y_positive)
        neg_coef, y_negative_regressed = text_analysis.linear_regression(x, y_negative)


        plt.sentiment_plot(result,
                           y_positive_regressed,
                           y_negative_regressed,
                           pos_coef[0],
                           neg_coef[0],
                           participant,
                           result_dir)
        #break


    #####################################################################
    # participants_sentiments = {}
    # participants_sentiments_values = {}
    # for participant in ["linda", "don", "christine", "katie"]:
    #     participants_sentiments[participant], participants_sentiments_values[participant] =\
    #         text_analysis.sentiment_per_session(participant)
    #
    #     participants_sentiments[participant] = sorted(participants_sentiments[participant], key=lambda tup: tup[0])

    # participants_sentiments = {'katie': [(1, {'positive': 0.3142857142857143, 'neutral': 0.45714285714285713, 'negative': 0.22857142857142856}), (2, {'positive': 0.27631578947368424, 'neutral': 0.4868421052631579, 'negative': 0.25}), (3, {'positive': 0.2631578947368421, 'neutral': 0.6491228070175439, 'negative': 0.12280701754385964}), (4, {'positive': 0.2875, 'neutral': 0.6125, 'negative': 0.1125}), (5, {'positive': 0.36486486486486486, 'neutral': 0.47297297297297297, 'negative': 0.17567567567567569}), (6, {'positive': 0.3541666666666667, 'neutral': 0.375, 'negative': 0.22916666666666666}), (7, {'positive': 0.20754716981132076, 'neutral': 0.6415094339622641, 'negative': 0.1509433962264151})], 'christine': [(1, {'positive': 0.3076923076923077, 'neutral': 0.5384615384615384, 'negative': 0.15384615384615385}), (2, {'positive': 0.1282051282051282, 'neutral': 0.5641025641025641, 'negative': 0.3076923076923077}), (3, {'positive': 0.25, 'neutral': 0.5416666666666666, 'negative': 0.20833333333333334}), (4, {'positive': 0.28, 'neutral': 0.52, 'negative': 0.24}), (5, {'positive': 0.3, 'neutral': 0.55, 'negative': 0.25}), (6, {'positive': 0.34615384615384615, 'neutral': 0.4230769230769231, 'negative': 0.23076923076923078}), (7, {'positive': 0.10344827586206896, 'neutral': 0.7586206896551724, 'negative': 0.13793103448275862})], 'don': [(1, {'positive': 0.125, 'neutral': 0.65625, 'negative': 0.21875}), (2, {'positive': 0.12857142857142856, 'neutral': 0.5571428571428572, 'negative': 0.3142857142857143}), (3, {'positive': 0.26666666666666666, 'neutral': 0.5333333333333333, 'negative': 0.2}), (4, {'positive': 0.1935483870967742, 'neutral': 0.5161290322580645, 'negative': 0.2903225806451613}), (5, {'positive': 0.25925925925925924, 'neutral': 0.5185185185185185, 'negative': 0.25925925925925924}), (6, {'positive': 0.2, 'neutral': 0.4, 'negative': 0.4}), (7, {'positive': 0.14285714285714285, 'neutral': 0.5714285714285714, 'negative': 0.2857142857142857})], 'linda': [(1, {'positive': 0.2608695652173913, 'neutral': 0.5652173913043478, 'negative': 0.17391304347826086}), (2, {'positive': 0.08571428571428572, 'neutral': 0.5142857142857142, 'negative': 0.3142857142857143}), (3, {'positive': 0.35, 'neutral': 0.4, 'negative': 0.25}), (4, {'positive': 0.25, 'neutral': 0.6666666666666666, 'negative': 0.16666666666666666}), (5, {'positive': 0.5, 'neutral': 0.40625, 'negative': 0.1875}), (6, {'positive': 0.29411764705882354, 'neutral': 0.5882352941176471, 'negative': 0.17647058823529413}), (7, {'positive': 0.1875, 'neutral': 0.59375, 'negative': 0.25})]}
    #
    # plt.participants_sentiment_plot(participants_sentiments, result_dir)

