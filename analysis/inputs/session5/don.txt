[00:00:00]  

R: Good morning John. Welcome to session 5. How was your weekend? How is your weekend? 

P: It was good. I got to get my hearing aids fixed. 

R: Great. Did you do any pleasant activities like the ones we talked about last time?  

P: Well, I didn't do too many. Most thing that I was when I went out with the grandson. Then he came and visited us for a while. A couple hours. That’s about all I did. 

[Ryan froze, had to restart session] 

R: Good morning, John. Welcome to session 5. 

P: My weekend was fine. 

R: Did you do any pleasant activities like the ones we talked about last time? 

P: I don’t think I did any of the ones we talked about. I just went out with the grandson. And he visited me and his wife for a couple hours. 

R: How fun. What did you do? 

P: What did I do? Watched TV and out the window and listened to the music. That’s about it. 

R: What did you do with your grandson? 

P: Well we went out to eat for breakfast and then we just, we just visited when he come back, and then his wife. They stayed for a couple hours. Talked about a few things. 

R: That sounds like so much fun. 

P: It was. 

R: Talking is great. 

P: Yep. 

R: Good. I’m glad you did that. Tell me about what you did and how it went. That sounds amazing, Don. I'm really happy you did that. Hopefully you remembered to record the activity on your pleasant activities worksheet. [00:03:20] My hope is that in doing so you would see that even if we don't think we're going to enjoy an activity beforehand, we often have more fun than we thought we'd have. Great work. Since we're still talking about activities. I thought it might be a good idea to discuss goals. when you're depressed. We often lose sight of our goals so it's critical to set achievable goals. These goals can be an career or career. We want to avoid career goals because more concrete goals are more attainable. For example, if I were to say My goal is to spend 10 hours a week practicing my instrument do you think that's a career or any career goal.  

P: It would be clear? 

R: Good job. You are exactly right. Not only did I mention it girl but I also outlined quantifiable steps to reach that goal. Let's try another. If I were to say My goal is to spend three hours a week spending time with my friends do you think that's a career or career goal.  

P: Clear. 

R: Perfect. You're right. Not only are career goals important but you want to also try and set realistic goals. Keep in mind that what's realistic at one point may be unrealistic at another time. If you feel you can't meet most of your goals they're probably unrealistic for that time. You can make it more realistic you'll achieve them by developing a plan. You cannot break down large goals into smaller steps for example. Since we are talking about goals and planning you should tell me some of your goals. What are some of your goals? 

P: You got me. 

R: What are some of your goals? 

P: I don’t know. I can’t remember – what I should be doing. Some of my best goals I guess is meeting people and talk to ‘em. 

R: I see. What are some obstacles you feel are preventing you from reaching those goals? 

[00:05:54] 

P: Not really nothing. I just don’t do it. I should be doing it. 

R: I can understand how that would be difficult. I know for me time management is something I really struggle with when it comes to my goals. A good way to help with time management is to assign each item a priority the highest priority items will come first. And if you don't have room for the others in your schedule you'll just do the highest priority items. Just make sure when you do this that you make sure you have a balance between what you must do and what you want to do. Now I want to shift our focus slightly and talk about control when we are depressed. We don't need no control. Can you think of anything you can do to feel more in control? 

P: I guess the only time I'm out of control is when I think of my wife. [And if I did/or maybe her name?] I think of her too much. I guess I can’t forget her. That’s what makes me feel bad 90 percent of the time, is when I think of her. 

R: I understand that hurting a healthy amount thinking about. 

P: I think about her most of the time. 

R: Remember the good times but don't let it bring you down.  

P: Yes, we had a lot of good times. We had a good life together. So many years, I guess it's just hard to forget.  

R: You don't need to forget, just adjust. Good. Let's continue. In addition, you can try and look at alternatives in your life these alternatives can include positive activities positive social contacts and more healthy thinking. The main idea to remember here is that there's always another path you can pick in your life. [00:08:22] You can choose an alternative activity that is more positive than what you are currently faced with. You have choices can really help you feel more in control. In addition, you can think about life as being made up of chunks of time. I want to share with you a quote in regards to this. I'm display it on your tablet. What did you think about that quote? 

P: That's a good quote. 

R: Interesting thought, Don. I like the last line very much as it’s so true. I want you to always be the one deciding what you do with your time and not anyone else or anything. You are a valuable person and your time is equally as important. In regards to the time you have available. What do you feel gets in the way of you enjoying your life more?  

P: I guess it’s just myself. I don’t get out and do things like I should. 

R: That’s okay. We can work on that. 

P: I guess I will. I have to. 

R: I know you can do it, and I’m here to help. We all are. I see. How have you tried to deal with this? Has it helped? 

P: Yes. 

R: Good. I’m happy to hear that. Why do you think it has helped? 

P: I didn’t hear the whole question. 

R: Why do you think it has helped? 

P: Well because you tell me a lot of good things to do. Some things I do. Not as much as I should. Or as you want me to. 

R: Good. That makes me happy. Any progress is good progress. Great. That's amazing. I'm glad that helps. Remember that to feel more in control. You can always look at some additional methods. to bring this conversation full circle. [00:11:11] What are some alternatives or other options available to you.  

P: I don’t know. 

R: Perhaps we could focus on making a Schedule.  

P: Yes… 

R: Good job. Remember that no matter what there always be alternatives you can turn to in any situation you've been doing great today. I want to end today with one more pleasant activity. We haven't discussed yet. Music. I remember you telling me you like bluegrass music. This is a song I thought you might like. 

P: That’s great.  

R: What did you think of the song and how did it make you feel?  

P: It makes me feel happy! That song was great! 

R: It’s great. You really like that music? 

P: Yes. 

R: That’s great, Don… I really like that music. Yes that's great. Darn I absolutely love this type of music. It makes me so happy. This session is actually our last one about how our activities affect our moods. Hopefully you've learned it as you spend your time in a more satisfying way. Your life will become more satisfying to you can you go pleasant activities into our life plans to help us feel less distressed and emotionally healthier and the better you feel the more you can do for yourself and others. I'll see you next time. Goodbye.  

 

 
