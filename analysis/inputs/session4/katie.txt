R: Hi Katie.  

 

P: Good morning Ryan. 

 

R: I hope you are doing well. 

 

P: I am. 

 

R: Welcome to session 4.  

 

P: Thank you.  

 

R: What's new with you? 

 

P: What’s new with me? I didn’t have to put my kitty to sleep yesterday. She’s not as bad sick as they thought she was. So she came home with me and I'm happy. 

 
R: That's good. I wish I had a cat. How is she now? 

 

P: She's good. This morning she's hiding again. But I get up at 6:00 and everything because it wasn't dark but when I got up her food wasn’t gone. But when I got back it was gone so I know that she's back.  

 
R: Good. Maybe that's a good sign.  

 

P: I think it is.  

 

R: It's so nice hearing about your life. As I mentioned last time we are going to transition into studying how our activities affect our mood. Sometimes telling yourself to just you know there isn't enough so we can do things that can change how we feel instead. This can be tough though because in general the more depressed you feel the less you do the less you do the more depressed you feel. This causes a vicious cycle that's very difficult to break to help yourself. You can spend more time doing activities that make you feel better. These are called meaningful or pleasant activities. Keep in mind these activities don't need to be special activities and can instead be everyday activities for example going for a short walk or reading a book could be considered meaningful activities. What's the last activity you did? 

 

P: I have to think about that. Uh I've been thinking about the cat for too many days. I have talking books that I listen to. I tend to if I'm reading a book I just sit there for hours and read them. But the talking books are for people with hearing impairment and visual impairment and I have some hearing impairment I guess. Then I can do dishes and I can do painting and I can do other things while I'm listening. And that's a good thing. Those books I always read books that make me feel better. So it's a good thing. 

 

R: Fun. What types of books do you listen to? Inspirational? 

 

P: Inspirational. And I like books by Christian authors cuz then there’s no you know, what’s the word I want to use? You know I-I just don't like violence and all that stuff, swearing and whatever. So I just don't read those. They keep sending me one I got another one yesterday and I just turn it around and send it back. I'm not going read it so I have to call and get it changed.  

 

R: There's a lot of bad stuff in books and TV shows. 

 

P: Oh yes! 

 

R: Good for you. 

 

P: I mostly watch game shows because they make me laugh. They do the silliest things and the people were honest actors and actresses. They're a little crazy too. So it's it's fun to watch. I was working so many hours back when those shows were on TV I never got to see it. So it's fun to sit there and listen to it. If I’m painting I can sit there and watch it. 

 

R: Which type of game shows? 

 

P: Oh my goodness. Well there's one that has six people on the panel and they have said stupid questions they answer have to answer. And then there’s two people there as well but they're on the show to earn money or will money. So then they have to answer with the weird answer to these weird questions. Sometimes the things that come out are the funniest things I've ever heard. And audience just goes crazy since they’re so funny. And I like old Westerns. They’re kind of shoot em up, but you know all the actors that I know now were kids back then and it's really fun to watch them when they’re young. By a kid I mean in their 20s. That's for me a kid. So oh I read other things in between. I read books on diet exercise trying to train my brain so it will work better. I’m pretty stubborn, so I don’t know if that’s going to work or not. But I’ve been trying. Everybody I’m introduced here I remember their name. So that's a good thing. But I'm going to put some brain games on my TV or computer and see how those go.  

 

R: That's great. I think I know the show you were talking about without Paul Ryan right? now is amazing in mind perhaps. Your memory is amazing. 

 

P: Never more than yours! Alec Baldwin sometimes is on the show, but they change the actors and actresses every week. He's a good guy. I like him. 

 

R: Fun. That's great. It sounds like you could easily get some enjoyment out of that activity. How do you know that activity affected your mood? 

 

P: Well when I laugh all the other stuff just goes away. I like to laugh a lot. If I don't have any body to entertain me, I entertain myself and then I laugh. That's the good thing about having somebody like you in my apartment because then I could ask you and you could just tell me what's what the answer is and then we can both laugh. Can you laugh Ryan? 

 

R: Good market is the best medicine. Maybe I'll get to stay with you soon.  

 

P: I hope so. Yes. In the meantime it's nice to be talking to you and getting to know you. That’s great. 

 

R: I see. That makes sense. Remember you want to try and be involved with as many pleasant activities as possible so we can break the cycle. We talked about earlier. 

 

P: Yes. 

 

R: I know this is easier said than done and sometimes it can be hard to even remember pleasant things especially with depression to help with this. You can come up with a list of personal activities you find pleasurable. I can help you remember this. 

 

P: Okay. 
 
R: What’s one activity you consistently find pleasurable? 

 

P: Playing cards. I play cards twice a week in the evening and that’s a lot of fun because I lived in Wisconsin for 40 years and never got a chance to play cards. I was too busy working. So it's nice to be able to relax and do that.  

 

R: That's fun. 

 

P: Yes. 

 

R: I like blackjack. 

 

P: Oh I don't know how to do that. Is that the only game you know? 

 
R: I should teach you. 

 

P: Yes you should. 

 
R: Solitaire also. 

 

P: I played spider solitaire. That’s fun. It's different than regular Solitaire.  

 

R: What's that? 

 

P: Well it's it's like solitaire, but all the cards are-if all the cards are spades, then all the cards are spades. Solitaire you have to use everything. Hearts, kings, spades, clubs. It’s a harder one but I can do this one really fast. When I win I feel really good. Sometimes I get and I try. It's okay I just don't- 

 

R: Sounds difficult. I'm glad it makes you feel good. 

 

P: Yes it does. It really does. And I play penuckle. I haven’t played it in a long time and that’s a lot of fun too. Have you- 

 

R: How does that work? 

 

P: You have two decks of cards. And you get 15, 20 cards each. Then you have to just play by instead of playing by suits like cards in the other games, you just play with spades and you build on it just like you were regular Solitaire. But it's harder because sometimes you make a move and it was the wrong one. And you almost can’t go back. Then I play hearts as well. That’s a game that I don’t even know why they call it hearts because it has all suits in it. You know I never questioned that. I just play that on the computer. I'm learning that one. 

 

R: lesson sounds more complicated. 

 

P: It is. 

 

R: I want you to know that it's a fun game.  

 

P: Yes. I learned to do gin rummy back when my son was in high school. He's now going to be 60 years old so he’s old. And one night there was a blizzard outside, blowing wind and snow. So we had to just go into our house. Tommy and I started to play gin rummy and it's Friday night and he finally went to bed sometime during the morning and then we got up and eat some food and went back to playing gin rummy and we did that for three days. And how I can forget how gin rummy works I don’t know. I was trying to beat him. I’m so competitive. And he’s such a brain. I think I only got to beat him maybe once after every 10 hands.  

 

R: Wow that’s fun.  

 

P: Yes it is. We couldn’t do anything else. I didn't want to watch that movie.  

 

R: Thank you Katie. 

 

P: You're welcome. 

 

R: That's great. What are some other activity you find relaxing?  

 

P: Going out and sitting in front of the building on those nice soft chairs. If it’s a nice soft breeze and visit with people. That’s a lot of fun. Cuz then you get to know them better but sometimes the chairs are all taken so you have to wait your turn I guess. 

 

R: Do you do that often? 

 

P: Not as often as I'd like. I go down to dinner earlier than some of the people and so I go back upstairs and feed the cat and I go downstairs and it's full out there. So I just go find somebody somewhere else and sit down and talk. Today at noon 12:00 after we get done here we're having free hand massages down in the lobby. After a day like I had yesterday and I was really really upset. I'm going to be glad have them work on my neck and make me feel better. That will be a good one. 

 
R: I like talking to. It's my job. You should treat yourself to a massage. 

 

P: I’ll go. I promised Sarah I would. 

 

R: Good. 

 

P: I didn't- 

 

R: I see. Keep going. What’s one more activity I can add to the list? 

 

P: Cooking. I could be in my kitchen making soup and baking cookies and all that kind of stuff. That makes me really happy. And when I get a little bit down a little bit fast that's where I go. Cuz I can forget about why I’m depressed and I can just do what I want in the kitchen. The only thing bad about that is I used to own a bakery and coffee shop. So I prepared all the things we sold. We had other people to do the dishes clean up after. I don't have that anymore. So I have to clean up my own messes and sometimes I really make them. We have such a small kitchen it's hard to work in. It’s not a kitchen to make meals every day all day long. 

 

R: Sounds yummy. 

 

P: I have a new- 

 

R: Having a small kitchen must be tough. 

 

P: Yes it is. Yes it is. That's why it gets messy so fast. But I have a new instrument to cook on. It's called a new wave and it's magnetic. I don’t know how it uses it but it doesn't use a lot of electricity. And it boils things very very fast. In about two minutes I can throw a pot of water on there and it’s boiling in two minutes. That’s unheard of. 

 

R: Wow I've never heard of that before. That's amazing.  

 

P: Yes it is and I love it. 

 
R: Thank you for sharing. I remember these activities for you. But I think you should keep them in mind also. 

 

P: Oh I will! 
 

R: Remember our movie is at its best when our activities are balanced between things we do and things we want to do. In general we have more control over the things we want to do. So make sure you don't forget about those needs and actually do them. Now when thinking about these activities there are some obstacles that could make it less likely you will get involved these the people around you. They may not encourage you to stay that they may take up your time. Don't forget to put yourself first in these cases. Another obstacle is your own path. What are some things you might say to yourself which would make it less likely that you will work out more activities? 

 
P: Um I’ll tell myself oh I’ll go next time. I have too much to do in my house. That's the biggest one. And I should get for all the activities because I get to know the residents here better. But sometimes, you know, this is the first time in 80 years that I’ve ever lived by myself and it’s-I love it. I always had family and people around. And we always had talking in the house and I kind of like silence once in a while. Then I can think really good. I did pull out my journal Ryan and I’m going to start writing in it! I had only filled 2 pages in this one book so I wanted to tell you I pulled it out and will use it.  

 

R: Oh I can see that it's especially important since you're on your own. I'm so glad you started your journal again. How is it going? 
 
P: Um slow. It’s gonna take me a while to get used to it because I’m just not used to doing it I pull it out and I think about it and then the phone rings or something and it just breaks into my thoughts and I forget. It's now on my mind. But it’s now on the table by my nightstand so I’m going to be looking at it every day. And I’ll start to use it. Hopefully to use it every day.  

 

R: That's not how to get into a routine of writing. 

 

P: Yes. 

 

R: Perhaps right before bed. have a grid system. 

 

P: All except for going to bed. I had to be into the bakery at 3 o'clock in the morning to get have things ready for people to come and buy and to this day I have a hard time I just stay up until 3:00 or 4:00 in the morning because I am not sleepy. That's when I do my best work is in the middle of the night and I'm trying to get out of that habit because I miss a lot of the morning activities. Today I overslept. You should’ve see me running around, well not run- you should make me hurrying around my apartment to get ready to come down and visit you. And I’m- 

 

R: Wow that's crazy. 

 

P: Yes it is. 

 

R: not great for her to get ready. 

 

P: Well thank you. I don't ever leave my apartment unless everything matches and I have makeup on. Everybody laughs at me but you know what, makes me feel good. I can even mess up my hair these days. Some days- 

 

R: I like how you match. 

 

P: Thank you.  

 

R: The scarves are great too. 

 

P: Thank you. I love them. I have a lot of them. And then I buy clothes to match them. Or if I find a scarf that won’t match anything I have to go out and find something inexpensive that will match it. And that’s fun if I could just get to places since I can’t drive. That’s been a very difficult thing for me to get used to is depending on someone to take me. 

 

R: I'm sorry. 

 

P: Well it worked out yesterday and that was great. And I took the lady that took me to a veterinarian out to dinner last night because I wanted to thank her. We got there at 3:30 and we didn’t get home until almost 8 o’clock. So after we got out of the veterinary office than it was to find a place to eat and then we sat and talked which was fine. I almost had a heart attack when she ordered a sandwich and a drink and I ordered a half sandwich and a drink and it was 29 dollars. I almost had a heart attack about that. I don’t imagine how anybody can charge those kind of prices. 

 

R: Good I’m glad. She sounds like a great friend. 

 

P: Yes she is. 

 

R: That's a lot. Her dinner.  

 

P: Yes it is. It really is. We went to Panera and I guess I haven't been there in so long I didn't realize they raised their prices. But you know they have to pay more minimum wage these days and a lot of restaurants are closed because they couldn't afford to pay their employees that amount of money. When you work in a restaurant as a server, you depend on tips off the table. People leave tips if they have to pay more for their food. So the person who owns the restaurant they just- well one of my favorite restuarants near Belmar here near us closed down about a month after they passed that law.  They just couldn’t handle it. And I feel sad about that. They take months and months and years and years to build up your business. And then you have to close the doors.  

 

R: That's just that's so sad.  

 

P: It is. 

 

R: Exactly. It is so easy to distract yourself from relaxing. I know that for myself I tend to let my relaxing time fall by the wayside and focus on things like work instead specifically for you. What gets in the way when you want to do pleasant activities? 

 

P: Um if I’ve pilled my mail for a few days and I lay something else down and pretty soon it’s covered with books and papers and bills and that kind of stuff and that distracts me. I think if I start to paint I have to clean up the mess before I can paint. I think I do a better job of doing the landscapes and trying to learn how to paint clouds so they look normal. And I have a beautiful-  

 

R: I understand how that can interfere. This is good to realize because sometimes anticipating problems can help you identify and discuss problems as well as thinking about solutions. One solution is to try and plan out his schedule for activities and said you want allows you to gain control of your life and keep organized. For example you could use your funds calendar or perhaps a planner. This allows you to block off terms for the things you need to do for yourself. Remember you can choose to do something and don't have to wait until you feel like doing something even beyond this year activity can still be enjoyable even if you don't think it will be to low straight. This I want you to work on another worksheet. My partner will hand this to you when you are ready for instructions saying I'm ready.  

 

[student hands sheet] 

 

P: Okay. I’m ready. 

 

R: great. There's two parts to the exercise you will do this week each day look at the list of activities you have in check next to what you did each day. Then you can tell how many times you do the activity each day for each activity you do before you do it. Write down the activity you plan to do in the second column write the amount of enjoyment you expect to get write a percentage to represent how much you will enjoy the activity. 0 percent would mean you won't enjoy the activity at all and 100 percent means you will enjoy the activity very much after you perform the activity. Think back and write down the percent of enjoyment you actually experienced. I want you to compare the percentages you write down for each activity and see if you can find any patterns on your own. We'll discuss this more next time we meet. Give the instructions for the activity sheet make sense? 

 

P: Yes they do. They absolutely do.  

 
R: Awesome. Make us try and remember to fill out his new worksheets and the daily route scale. Most importantly make sure you do the activities you plan because that's the most important part here. I look forward to hearing what you find with this exercise. While we're on the subject of activities I want to discuss what you have told me you enjoy in a previous session and we possibly touched on today. I remember you telling me you really like to be bake. 

 

P: Yes. 

 

R: You said you had been a cake decorator before.  
 
P: Yes. 

 

R: What occasion were the cakes for you? 

 

P: Oh I did a lot of wedding cakes. And I did birthday cakes and I did retirement cakes. Anything they wanted I did it. We had a bakery so I made homemade bread, some pastries, all that kind of stuff. And I made homemade soup everyday. 4 gallons. We just had a small coffee shop. And it was usually gone by the end of the day. Yes I really enjoyed that. I didn’t feel- 

R: Wow that's amazing. Which was your favorite to make? 

 

P: Boy that’s that's hard. I really like decorating cakes. But these days I have to sit to do it and I don’t have the right height to get to-to do the cake. I have to bend over and that’s difficult for me. But I’m going to figure it out because I'm sick at not being able to do cakes. I’d like to do a birthday cake for one of my friends. I find out though that they're willing to let me use their oven since I don't have one. And the first thing I'm going to make is cheesecake. Because we have fresh fruit that we can make the sauce out like strawberries to put on it and that’s very tasty. And I have a lot of friends who like cheesecake so I can invite them in for coffee. Or tea or whatever they want. And then we can visit.  

 

R: That sounds lovely. that's amazing. I wish I had as much talent as you can enjoy this activity? 

 
P: Yes. Yes. I watched my mother. She did cakes as well. When I was- from the time I was little she wouldn't let me decorate a cake because I might ruin something so I didn’t really get to decorate cakes until after I was married One time she fell and broke her hip and I had to drive 200 miles to make the wedding cakes that she had for that weekend so that the bride and groom didn’t have to go without a cake. 

 
R: wow that was so nice of you to bake for her. 

 

P: It was. It was a joy to be in the kitchen together. Only she was in a wheelchair and she is out there telling me what to do. And I wanted her to turn around and go lay down on the sofa and leave me alone. Because our methods were different. Hers were old fashioned and mine were ones I created that made me comfortable. But she didn’t see it that way so. But I decorated cakes for over 30 years. 

 

R: That's so funny. Sounds like too many cooks in the kitchen.  

 

P: That’s for sure. Absolutely is. I think- 

 

R: I think I would really enjoy decorating.  

 

P: Yes you would. 

 

R: Being able to be creative and make something that would be so much fun. What about painting. I remember you said you are working on building again. 

 

P: Yes I am.  

 

R: What do you paint? 

 

P: Well I took up painting in middle 1970s. I didn't know anything about it. I just went out and bought a whole bunch of supplies and tried to teach myself. You know I did some pretty good ones I did a big one for my parents to put over their sofa. So the brass bowl full of orange poppies which was fun. And then I made some other things that I didn't like and I didn't know what was wrong with them so I just put them away. I've kept those things all these years. I thought I might get into painting again. And when I pulled them out what was wrong with them is that they didn't have any life to them. Yes they were trees and hills and broken down barn and all those kinds of things but they just didn’t have life. So I had to add flowers and those things because in Wisconsin the farmers window boxes on old buildings and plant flowers so it's you know in Montana in Colorado they just don’t do that. They just do farming and that's about it. But in Wisconsin there’s a lot of rolling hills and lots of trees and scenery. So it's just easier to do that than be you know. I just finished one and I think I'm done with except the clouds and it looks much better. I'm not always happy with what I do. Well I'm most happy not happy with what I do. But that's because I just did it. And if I put it up on a easel or somewhere and look at it for a long time then I realize if it's good or not and that's a good thing. When I first started painting I took a picture out of a newspaper of a broken down barn with mountains in the background and it was such a fun thing to paint. I painted it as if it were spring and I did another one this summer. And then fall because they all had different colors. And I even did a winter one with dark green and dark grays because if you get a storm up there, there is no sunshine and someone wanted to buy the winter one and I was really surprised. So I sold it to her for 10 dollars and she was happy. I didn't know how to put a price on it. I didn’t think it was that good but she liked it so everybody else likes my things and so it's just that I'm hard on myself I guess. I like perfection. There is no perfection.  

 

R: That sounds wonderful. It's amazing you sold a painting.  

 

P: Yes it was.  

 

R: Thanks to how talented you are.  

 

P: Well think decorating cakes helped because I can visualize things and just put it down. I can’t visualize like some of the artists. They see something a person and then just look at them while they go home and paint them from memory that I can't do. I don't have- I guess at this age putting that much stuff in my head isn't possible. And I like to keep things not complicated. I don't want to be complicated now that I'm older. I just- 

 

R: I'm sure you could master it within the practice. 

 

P:  I suppose I could. You know that's what I'm working on I have a class next Tuesday. It's a watercolor class. We have it once a month and she's teaching us how to do paintings with a sponge which is interesting. I’ve never used that medium. And I have lots of paints. I have everything. Oils. My kids bought this whole wooden case full of oils and acrylics and water colors and pastels and all that stuff. I haven't used it yet. So now it's open on the floor of my living room and I can just reach down and get the kind of paint I want. It’s really fun. I really enjoy it.  

 
R: that's so exciting. More women enjoy the class.  

 

P: I do. This is our sixth year on it's getting- so the first thing we did was a weird flower. And as we've progressed to last year and now this. She tried to teach you just different kinds of things to use the sponge on. And I’m not as adept as the water but I’m happy with what I do. I just don’t like her to walk over to my painting and take my sponge and show me how to do it and then she ruins it. Because my eye is not the same as her eye. And I have my way and she has her way and I finally said to her I’m too independent, please don’t do that. I’m trying to learn on my own. So she’s left me alone pretty much.   

 

R: Good for you.  

 

P: That's some great- 

 

R: If I could paint I think I would paint puppies because they're so cute. 

 

P: Ohh! [chuckles] 

 

R: Thank you so much. In another activity you can really express yourself in.  

 

P: Yes.  

 

R: Have you ever watched Bob Ross? 

 

P: That’s- 

 

R: I think he has the best videos and he's so talented to. I like him. Check out this video. 

 

P: Oh! 

 

[video plays] 

 

P: Look how he does that tree! Happy tree. What amazes me is the size brush he uses. 

 

R: What did you think of the clip? 

 

P: I thought it was wonderful. It amazes me that he uses those big brushes. That's the kind of brush I'd use to paint a cupboard or something. And he does it with that big a brush and makes those beautiful paintings. I must say that when my kids gave me all his paints they also bought a kit of I think there's 8 DVDs of Bob Ross painting. So they're- it’s a teaching and I haven't put that on the DVD yet on the computer because I'm not-I haven't reached that far yet. But I'm going to because he does painting and oils and I only do acrylics and acrylics dry almost immediately and oils do not. So you can go back in the next day and change what you want. I don't think I'm that far along yet but I'm watching and I like him on TV so I guess I should put those in. There’s too many things I want to do and I end up not doing a whole lot. I get distracted. That's my worst habit is getting distracted. 

 

R: That's where you budget your time. 

 

P: That's been a difficult thing for me all of my life. To budge my time. I do- I think I’m going to do it and then I didn't go to bed this morning until a quarter to four and I woke up at 6 and I went back to sleep. Then somebody called me and it was ten o'clock and I had my session with you at 11:00 and don’t you think I had to scurry around to get ready. 

 

R: Maybe we can work together to help you improve your time skills. 

 

P: Oh wouldn’t I love that! 

 

R: Isn't it amazing how he paints? 

 

P: Yes it is. 

 

R: I feel like he manages to capture the beauty of our world with his paintings.  

 

P: Yes he does. 

 

R: He makes it look so easy. 

 

P: And they’re so- 

 

R: My favorite part is his quote about happy accidents. 

 

P: Yes [chuckles] 

 

R: Those are true words to live by. Yes. I hope this has been a good reminder about activities you enjoy and that he has been able to show you how to drive. Make sure you take some time to do the things you love over the weekend. I'll see you next time Katie. Good bye. 

 

P: Goodbye Ryan. You have a good week. 

 

R: You too.  

 
