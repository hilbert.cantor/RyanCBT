R: Hello Christine.
P: Hello, oh-
R: Welcome back today we are going to start our second session together and begin I want to know how using the daily mood scale went. Did you have any problems filling it out. 
P: Yes I was sick most of the week. 
R: That's okay I understand. Great. What did you find surprising when filling out the scale?
P: Nothing much really.
R: I see. Thank you for your work with the scale, I hope you can use it as a tool to reflect on your thoughts. Keep it up over the coming weeks. Now to continue with our session. Remember from last time. Thoughts, activities and people affect your mood. We are going to start by focusing on what role thoughts play in your mood. Specific types of thinking can make a huge difference. So make it more likely you will be depressed and others make it less likely you will become depressed. How would you define thoughts?
P: Um they indicate how you’re feeling.
R: Thank you. I'll tell you what I thought. To me, thoughts or ideas we tell ourselves. In fact we talk in our own heads all the time even if we are not always aware of it. It's helpful to think of thoughts as things that can have a real impact on our bodies and minds depressed thoughts and ways of thinking, have very distinctive patterns there often inflexible and judgmental. We should strive to have flexible thoughts because they can help us from being depressed. For example, it wouldn't be flexible to think I'll never get better trying to think of a way you could rephrase. I'll never get better in a flexible manner?
P: Am I supposed to say it out loud? I don’t feel great now, but I’ll improve. That’s how I like to look at things when I’m feeling down.
R: Great. I love that you are doing great. Here's another way to rephrase it if I go to therapy I'm trying to feel better. We should try and look at nondepressed thinking is a goal. Nondepressed thinking has three common themes. The first one is that nondepressed thinking is changeable a lot of depressed thinking has a very definitive way of looking at things. Here's an example how can you restructure the statement. I always have been and always will be a coward to be more flexible?
P: I may be a coward now, but I’m going to improve. 
R: I see when I first heard that thought I would've said, I am afraid in some situations, some of the time. The emphasis here is on the word some the world is more dynamic than rigid, depressed thoughts lead us to believe another good thing nondepressed thinking does is looking at what we do rather than who we are as people. For example, instead of saying I was born to fields that you could say I am doing things that have me down right now is the good thing about nondepressed thinking is having hope for change depressed thinker may say something like nothing has ever helped me feel better. How can we rephrase nothing has ever helped me feel better to have more hope for change?
P: I look forward to feeling better because I’m working on my emotions.
R: I like that. Thanks Christine now you're getting it. I tried to rephrase the statement and came up with this. Nothing I've tried yet has helped but this is something new in the time might be right for a change. Now that we discussed depressed and nondepressed thinking patterns. Let's talk about how you can spot different types of thinking. One type is constructive versus destructive thinking, constructive thinking builds ourselves up and deconstructive thinking. Tears us apart. Let's practice the tablet you have been given to pick and say out loud. One of the choices. If I were to say I've made so many mistakes can you identify if it's a constructive or destructive thought?
P: Is it destructive?
R: Great job. That's the correct answer. This thought is more about making the person feel bad. Rather than building them up. Let's try another one which type of thought. Do you think it is. If I say I can learn to control my life to get more of what I want?
P: It’s a positive statement.
R: Good job that's correct because that thought aims to strengthen the person rather than the opposite. You're really getting the hang of this. Now we can look at another type of thinking. This type involves necessary versus unnecessary thinking necessary thoughts, remind us of things we to do such as a task or errand to run unnecessary thoughts describe things you can't change or that make us feel bad. Can you think of an unnecessary thought that has come across your mind?
P: I don’t feel well and I’m not going to get any better.
R: Let's keep going. Here's the one I came up with. There could be an earthquake. Soon, that thought is something neither of us need to worry about, especially because it is out of our control. The last type of thinking. I want to focus on is really important for our purposes, this type involves positive versus negative thinking positive thinking makes us feel better and negative thinking makes us feel worse and negative thought that could cross your mind is there's no use in me trying to feel better because nothing will change. Can you think of a way to rephrase this thought in a positive light?
P: I may not feel good now, but I’m going to get better.
R: Good job positively can really help the more you practice thinking this way, the more naturally these thoughts will come to you since we discussed some common types of thinking we should also consider some possible mistakes in thinking and learn how to recognize them. If these mistakes become persistent thought patterns there called cognitive distortions on your tablet you will see a list of the most common cognitive distortions which would you like to hear more about?
P: Polarized thinking.
R: Polarized thinking is when you assume everything is either black or white, or that there is no middle ground you put people, situations, and yourself into either/or categories to the extent that if something isn't absolutely perfect. It's a failure. Do you feel like polarized thinking applies to you?
P: Sometimes.
R: Great job on identifying a distortion. I know this one can make things pretty tough for you, so I'm glad we get to work on it today. The first thing I want you to do is think about a negative situation you're in now or have faced before. Go ahead and give it a try. 
P: My belongings aren’t going to get put together because my daughter will cancel on me.
R: Great. Now I want you to describe something you had to try several times before you succeeded.
P: Thinking positive in a negative situation.
R: Wow that's quite an impressive feat. I think it would've taken me much longer to succeed. Had I been in your situation. Now I would like for you to think of what would happen if you stop trying to succeed in that situation. [pause] You probably wouldn't have had as good of a mood right?
P: Right.
R: excellent. I would now like you to think of what would happen if you kept trying in your present situation and were able to succeed.
P: Mmkay. 
R: Great. Hopefully this thought experiment has helped you to see that stopping now may seem easier or the better course of action, but the reward for continuing to try his work at. How about you rephrase your original thought in a way so that you will want to keep trying again.
P: My daughter’s going to come over tomorrow and she’s going to put the rest of my furniture together.
R: That's great, Christine. Good job working on this distortion. Do you want to learn about another one?
P: Sure.
R: Okay, sounds great. Go ahead and pick another from the list.
P: Oh filtering.
R: Filtering is magnifying negative details while filtering out all positive aspects of the situation so that the negative details are all that you can see, do you feel like filtering applies to you?
P: Yep sometimes.
R: Great job on identifying a distortion is such a common distortion but once you identify it you can turn it around the first, I want you to do is tell me a negative thought tracking. 
P: I keep attracting jerks.
R: Great. Now I want you to think of one positive thing that you may be are taking into too much consideration right now. Tell me about it when you are ready.
P: I can learn how to set healthy boundaries. 
R: excellent. Now tell me another positive thought about the situation.
P: I won’t be attracting jerks.
R: Great. I know this can be difficult but think of one more positive thought about you or the situation.
P: I’m not going to attract jerks.
R: incredible. The last thing I want you to do is rephrase your initial thought in a more positive way. Now that you have identified some positive things about it. 
P: Okay.
R: How can you rephrase your initial thought?
P: I’m going to learn how to set healthy boundaries so I don’t attract jerks.
R: that's great, Christine. Would you like to learn about another distortion.
P: Sure.
R: Okay, sounds great. Go ahead and pick another from the list.
P: Did we already do overgeneralization? Okay overgeneralization.
R: overgeneralization occurs when you come to general conclusions based on a single piece of evidence or happenstance. If something bad happens once it is bound to happen over and over agent do you feel like overgeneralization applies to you?
P: Not so much.
R: Okay great job were nearly down the list of possibilities. Go ahead and pick another one to hear more about.
P: Um personalization.
R: In this case you believe that everything others do or say is a direct reaction to you. You may also believe that external events are caused in direct relation to something unrelated, that you did you feel like personalization applies to you?
P: No.
R: Okay great job were nearly down the list of possibilities. Go ahead and pick another one to hear more about.
P: jumping to conclusions.
R: when you jump to conclusions without any evidence or say, so you know exactly what will happen or whether a person is feeling and why they act as they do like fortune-telling you feel as though your predictions are fact feel like jumping to conclusions applies to you?
P: sometimes.
R: great job in identifying a cognitive distortion. Christine, when it comes to jumping to conclusions. You could be jumping to a conclusion about a person or situation. Which would you say is?
P: A person.
R: I see once it that you are assuming about this person? 
P: That they didn’t show up because they blew me off.
R: that's great Christine now tried to think of something else that this person may have been going through to give you that impression. Describe the situation, you may be going through to get to someone else. The impression that this person is giving you his understanding as possible.
P: They got busy doing other things.
R: good job thinking in someone else's shoes. This little thought experiment gave you an opportunity to explore other conclusions than the one you jump to by conducting thought experiments like this one. You can realize that the worst-case scenario is really the least likely great work you want to look at another distortion?
P: No I think we’re done.
R: sounds good. Great job today I went over a lot, but will keep reinforcing what you learned in future sessions. I'll see you next time. Christine goodbye.
P: Okay goodbye, have a nice weekend.
R: You too
