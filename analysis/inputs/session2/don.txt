R: Hello Don.
P: Hello.
R: welcome back. Today we are going to start our second session together and begin I want to know how using the daily scale went. Did you have any problems filling it out?
P: No. Not that I-
R: Great. What did you find surprising when filling out the scale?
P: No problem I guess after I know what to do.
R: I'm glad I see. Thank you for your work with the scale, I hope you can use it as a tool to reflect on your thoughts. Keep it up over the coming weeks. Now to continue with our session. Remember from last time. Thoughts, activities and people affect your mood. We are going to start by focusing on what role thoughts play in your mood. Specific types of thinking can make a huge difference. So the more likely you will be depressed and others make it less likely you will become depressed. How would you define thoughts?
P: That’s good I guess.
R: How would you define thoughts soon. 
P: Good is that right?

[student turns off fan to help participant hear, short conversation about hearing aids]

R: How would you define thoughts. How would you define thoughts.
P: How? I guess good.
R: Thank you. I'll tell you what I thought. To me, thoughts or ideas we tell ourselves. In fact we talk in our own heads all the time. Even if we're not always aware of it. I feel it's helpful to think of thoughts as things that can have a real impact on our bodies and minds depressed thoughts and ways of thinking, have very distinctive patterns there often inflexible and judgmental. We should strive to have flexible thoughts because they can help us from being depressed. For example, it wouldn't be flexible to think I'll never get better trying to think of a way you could rephrase. I'll never get better in a flexible manner.
P: I know I will get better.
R: You are doing great. Here's another way to rephrase it if I go to therapy I'm trying to feel better. We should try and look at nondepressed thinking is a goal. Nondepressed thinking has three common themes. The first one is that nondepressed thinking is changeable and a lot of depressed thinking has a very definitive way of looking at things here is an example how can you restructure this statement. I always have been and always will be a coward to be more flexible.
P: Well I think I’m doing good and keep moving and stuff like that. Should be taking more walks I guess.
R: Good. Do you like walking?
P: Yes.
R: do you walk here?
P: Not every day. Not outside every days. I go down for my lunch every day and walk around the house.
R: That sounds nice. I see when I first heard that thought I would have said, I am afraid in some situations, some of the time. The emphasis here is on the word some the world is more dynamic than rigid, depressed thoughts lead us to believe another good thing nondepressed thinking does is looking at what we do rather than who we are as people. For example, instead of saying I was born to feel sad, you could say I am doing things that have me down right now. The good thing about nondepressed thinking is having hope for change. A depressed thinker may say something like nothing has ever helped me feel better. How can we rephrase nothing has ever helped me feel better to have more hope for change?
P: I don’t know, I don’t- cuz most the time I feel good so I don’t worry about feeling better. I feel good most the time.
R: I'm glad you feel good often. Thanks, Don. Now you're getting it. I try to rephrase the statement and came up with this. Nothing I've tried yet has helped but this is something new in the time like be right for a change. Now that we discussed depressed and nondepressed thinking patterns. Let's talk about how you can spot different types of thinking. One type is constructive versus destructive thinking, constructive thinking builds ourselves up in deconstructive thinking tears us apart. Let's practice the tablet you have been given to pick and say out loud. One of the choices. If I were to say I've made so many mistakes can you identify if it's a constructive or destructive thought if I were to say I've made so many mistakes can you identify if it's a constructive or destructive thought?
P: I don’t know the answer.
R: [repeats question]
P: I know I’ve made a lot of mistakes, but I guess I’ve cured them all.
R: Great job. That's the correct answer. This thought is more about making the person feel bad. Rather than building them up. Let's try another one which type of thought. Do you think it is. If I say I can learn to control my life to get more of what I want?
P: I don’t know I guess just go about different things every day. I guess.
R: Do you feel that having control is positive?
P: Yes.
R: Good job that's correct because that thought aims to strengthen the person rather than the opposite. You're really getting the hang of this. Now we can look at another type of thinking. This type involves necessary versus unnecessary thinking necessary thoughts, remind us of things we to do such as a task or errand to run unnecessary thoughts describe things you can change or that make us feel bad. Can you think of an unnecessary thought that has come across your mind?
P: Well not really. I have pretty good thoughts most the time.
R: I'm glad that's very healthy. Let's keep going. Here's the one I came up with. There could be an earthquake. Soon, that thought is something neither of us need to worry about, especially because it is out of our control. The last type of thinking. I want to focus on is really important for our purposes, this type involves positive versus negative thinking positive thinking makes us feel better and negative thinking makes us feel worse and negative thought that could cross your mind is there's no use in me trying to feel better because nothing will change can think of a way to rephrase this thought in a positive light?
P: Well I guess I can’t. I don’t worry about things too much.
R: Good job thinking positively can really help you practice thinking this way, the more naturally these thoughts will come to you since we discussed some common types of thinking we should also consider some possible mistakes in thinking and learn how to recognize them. If these mistakes become persistent thought patterns there called cognitive distortions on your tablet you will see a list of the most common cognitive distortions which would you like to hear more about?
P: I don’t know, you got me there.
R: Is there one that interests you? 
P: I can’t think of one now.
R: How about filtering? during his magnifying negative details while filtering out all positive aspects of the situation so that the negative details are all that you can see, do you feel like filtering applies to you?
P: I guess so. 
R: shall great job on identifying a distortion filtering is such a common distortion but once you identify it you can turn it around. The first thing I want you to do is tell me a negative thought.
P: The only good thoughts I’ve got are I wish the wife was back. But I guess I can’t ever do that. But I guess she’s waiting for me. I told her I’d be there soon.
R: That's a good way to look at it. Great. Now I want you to think of one positive thing that you may be are taking into too much consideration right now. Tell me about it when you are ready.
P: I don’t know what to say.
R: perhaps you could consider your daughter?
P: Yeah.
R: Excellent. Now tell me another positive thought about the situation.
P: I don’t know too much about it. I know she spends too much time taking care of me. She’s got enough to do for herself.
R: that's not the case. I'm sure she loves time with you. 
P: Yeah.
R: That's a positive. Great. I know this can be difficult but think of one more positive thought about you or the situation.
P: I just take one day at a time.
R: Good me to does your daughter have kids?
P: I didn’t get that.
R: Does she have kids?
P: Does she have friends?
R: Kids
P: She has a lot of friends yes.
R: Incredible. The last thing I want you to do is rephrase your initial thought in a more positive way. Now that you have identified some positive things about it.
P: [no answer]
R: you should consider enjoying things here.
P: I do. I like it here. A lot of nice people.
R: good. Do you have friends here?
P: Yes.
R: great, that's great, Don. Would you like to learn about another distortion?
P: Yes.
R: Okay, sounds great. Go ahead and pick another from the list.
P: I don’t know.
R: Is there another you would like to hear about?
P: I guess I’ll-yes. 
R: in this case you believe that everything others do or say is a direct reaction to you. You may also believe that external events are caused in direct relation to something unrelated, that you did feel like personalization applies to you?
P: Yes I guess so. I would say yes.
R: Great job in identifying personalization as a distortion Don to work through this distortion. I would like you to tell me about a negative situation you have experienced that applies.
P: Not a lot of situations I’ve experienced.
R: Is there one you want to tell me about?
P: I guess I told you, uh most of them. A few times I was sick and stuff. Like that.
R: You were sick?
P: Excuse me.
R: You were sick?
P: Yes I’ve been sick a few times.
R: what happened?
P: I guess one of the bad ones was when I had prostrate cancer.
R: That's scary.
P: They gave me two years to live 30 years ago. I guess that was great.
R: Clearly you beat cancer.
P: Excuse me?
R: Clearly way longer than two years. 
P: Yes, about 30. 30 years.
R: That is amazing. 
P: And they say I’m cancer free. That’s good.
R: I'm glad great. Now I would like you to analyze your negative situation. Identify what about it you think is your fault.
P: You got me on those.
R: I don't think you have to worry there is nothing that is your fault.
P: I don’t worry too much. I don’t know if it will help or not.
R: Good. That is very healthy and does more harm than good. 
P: Right.
R: I see I know that it seems like this is dependent on you, but I want you to think about how this may not be the case. Think of some external factors that make this not dependent on you. 
P: [no answer]
R: Great work. Finally, please rephrase your negative statement in a way that your effect is minimized.
P: I couldn’t.
R: Okay I understand that's great Don. Remember what you think about yourself is solely dependent on you. You want to hear about another distortion?
P: I guess I could hear it. Maybe it will do me some good.
R: Sure, let's do another. Okay, sounds great. Go ahead and pick another from the list. 
P: [no answer]
R: Overgeneralization occurs when you come to general conclusions based on a single piece of evidence or happenstance. If something bad happens once it is bound to happen over and over agent feel like overgeneralization applies to you?
P: I would say so.
R: Way to go in identifying overgeneralization as a cognitive distortion you're already on the path to better patterns of thought. Tell me about a time you have overgeneralized.
P: There were probably a lot of times. I don’t remember. 
R: Ever at work?
P: Yeah everything else went well at work. I never had a job I dind’t like.
R: Where did you work?
P: Well I started on a dairy farm.
R: with cows?
P: Yes. Cows and feeding chickens and pigs and doing a little bit of farm work. Start at 6 in the morning and get done at 7 or 8 at night. 
R: I love cows. That sounds like a fun job but also tough. That's a long day.
P: It’s a good job. It’s hard, but it’s good.
R: I'm sure it's worth it.
P: It was for me. I didn’t farm all my life. I left the farm at one point and went to Jersey to work. Serving in the dairy farm bottling milk for 15 years and I wanted to go back to Pennsylvania and in-between I had a gas station for 10 years. When I made it back to Pennsylvania and then I worked for a doctor who had beef cattle and I took care of them for 20 years. Then he retired.
R: Did you miss the farm when you left?
P: Yes. 
R: I'm glad you were able to go back 
P: I was too.
R: we can continue good job it will be easy to think that a single instance defines you, but that's never the case. You are multifaceted person who has years of experience in life to define you. You just have to realize that the moment has passed, so you can forgive yourself for it. Easier said than done right, sometimes reevaluating a situation can help. For example, if your negative thought was I didn't do well with that job interview so I'm therefore horrible at job interviews. You can restate that as I didn't do so well on that job interview. But I'll be sure to do better next time. By changing that thought in your mind, you'll start to see how overgeneralizing applies trying to tell me about the situation again in a way that minimizes overgeneralization.
P: I don’t know. As I said, I liked every job I’ve ever had. Some people have jobs and they don’t like it. I liked all my jobs.

[pause while program loads]

R: your positive outlook applies well here that's great Don. Hopefully, this exercise helps you alter the way you think, for the better. Would you like to work on another distortion?
P: I guess.
R: Let's do one more.
P: Okay.
R: okay sounds great. Go ahead and pick another from the list.
P: [no answer]
R: when you jump to conclusions without any evidence or say, so you know exactly what will happen or whether a person is feeling and why they act as they do like fortune-telling you feel as though your predictions are fact do you feel like jumping to conclusions applies to you?
P: I would say yes.
R: Great job in identifying a cognitive distortion Don when it comes to jumping to conclusions. You could be jumping to a conclusion about a person or situation. Which would you say it is?
P: Right now it would be a person. My wife. I’m always thinking about her.
R: I see what's it that you are assuming about this person?
P: Just a person that I lived all my life with. Biggest part of it anyways.
R: How long were you together?
P: 67 years.
R: That's amazing. I'm sure you will get to see her.
P: I sure will.
R: I'm sure she would want you to enjoy life until then.
P: Yes.
R: good, that's great Don. Now try to think of something else that this person may have been going through to give you that impression. Describe the situation, you may be going through to get to someone else. The impression that this person is giving you as understanding as possible.
P: I don’t know what to say to that.
R: That's okay. Good job thinking in someone else's shoes. Hopefully this little thought experiment gave you an opportunity to explore other conclusions than the one you jump to by conducting thought experiments like this one. You can realize that the worst-case scenario is really the least likely great work you want to look at another distortion?
P: Nah I guess not today. I don’t think.
R: sounds good. Great job today. I know you went over a lot, but will keep reinforcing what you learned in future sessions. I'll see you next time. Don goodbye
P: Bye.
